/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package composition;

import java.util.List;

/**
 *
 * @author mymac
 */
public class Library {
    private String name;
    private List<Book> books;
    
    Library(String name, List<Book> books){
        this.name=name;
        this.books = books;
    }
    
    public List<Book> getTotalBooksInLibrary(){
        return books;
    }
    
}
