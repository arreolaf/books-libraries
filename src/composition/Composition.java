/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package composition;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mymac
 */
public class Composition {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Book b1 = new Book("Effective Java","Joshua Bloch");
        Book b2 = new Book("Thinking in Java","Bruce Eckel");
        Book b3 = new Book("Java: The Complete Reference", "Herbert Schldt");
        
        List<Book> books = new ArrayList<Book>();
        books.add(b1);
        books.add(b2);
        
        
        Library library = new Library("Central Library",books);
        
        List<Book>bks = library.getTotalBooksInLibrary();
        for (Book bk: bks){
            System.out.println("Title: "+bk.getTitle()+" and author is: "+bk.getAuthor());
            
        }
        
     
    }
    
}
