/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package composition;

/**
 *
 * @author mymac
 */
public class Book {
    private String title;
    private String author;
    
    
    Book(String title, String author){
        this.title = title;
        this.author = author;
    }
    
    public String getTitle(){
        return title;
        
    }
    public String getAuthor(){
        return author;
    }
}
